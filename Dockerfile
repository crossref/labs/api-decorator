FROM python:3.10-slim
RUN apt-get update; apt-get install -y curl unzip gcc python3-dev
WORKDIR /code
# set virtual env
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
COPY requirements/prod.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
EXPOSE 8092
COPY . .
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8092","--proxy-headers"]