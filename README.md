# API Decorator

## Motivation

The R&D group would like an easy way to deomonstrate how we can extend the API in various ways.

## Approach

In the spirit of "what s the simplest thing that could possibly work?", this is a reverse proxy that domonstrates how we can easily "decorate"  production API responses by proxying the request to the production API and patching the response with the new data before delivering it to the user.

At it's simplest it takes an API request of the form:

```bash
/<route>/<id>
```

And echos the request to the production API.

Once it gets a response, it then checks the `outputs.research.crossref.org` S3 bucket for any `json` files in the path `/annotations/<route>/<id>`, reads the json files in question and patches the response with the json before delivering the response.

So, for example, if you requested this singleton resource:

```url
https://api.labs.crossref.org/members/98
```

The proxy will:

- Proxy the same request to "https://api.crossref.org/members/98"
- Get the response
- Read every json file in `outputs.research.crossref.org/annotations/members/98/` and patch the response with the contents of each json file
- Deliver the response to the user

And in the case of a list resource, it just iterates over every item returned in `response['message']['items']` and does the same thing for each item.

So you could instead do:

```url
https://api.labs.crossref.org/members?query=Hindawi
```

And get get the same results.

## What does this mean in practice?

Well, for example, let's say we want to add more member data from "Sugar CRM" into every member record in the API?

To do so, we could simply use airflow to periodically query sugar for all member data and add a crm-data.json file to each member id under annotations/members directory like so:

```bash
/annotations/members/<member_id>/crm-data.json
```

And then any request to the `/members` route will add said data under a `crm-data` key for each item in the API response.

So again, lets say we want to add the following presevation data to the Hindawi (member id 98) record.

```json
{
        "all": 75,
        "cariniana": 0,
        "clockss": 5,
        "hathi": 0,
        "lockss": 70,
        "pkp": 0,
        "portico": 0
}
```

We would save it into s3 at:

```bash
/annotations/members/98/preservation.json
```

And then any query to the "api.labs.crossref.org/members" route that returned a response with the Hindawi record in it, would also include the preservation data for the Hindawi as well.

## Noyes on hadnling DOIs

DOIs don't work as s3 keys because they can contain lots of ccharacters that cannot be used in an s3 key. Other identifiers might have similart issues. In this case, we normalise the case of the DOI (lower) and md5 hash it instead.

Thus, to create an annotation `example-annotation` to the doi `10.5555/12345678`, you would save it using the folowing s3 key:

```bash
/annotations/works/7e10438b213c40d87373b48103dbadd6/example-annotation.json
```

## Extending functionality beyond just augmenting response data

But what if we want to add new functionality? For example, what if we wanted to add and entire new route? For example?

```bash
/match
```

Well, again, we could simply intercept the request and, instead of proxying it to the production API, we could proxy it to another sepcialised service.

## Limitaions

- Slow
- Stupid
- Funky
- Tests? What tests?

## Benefits

- Domonstrations. Allows us to demonstrate ideas and get feedback quickly.
- Decoupling. Allows us to build demonstration services that talk to a plausible API which would mean that, if the API ever did become part of our production service, it would be easy to repoint the demonstration services to use the real API instead.

## Could it be done more sensibly?

Probably. Maybe we'd prefer to use a proper reverse proxy or API gateway (haproxy, other) instead of hacking FatsAPI to do it? I don't know. I just wanted to demonstrate the copncept and get feedback. 

## To run locally for dev

- `python -m venv venv`
- `. venv/bin/activate`
- `pip install -r requirements/dev.txt`
- `uvicorn app.main:app --reload` (add `--host 0.0.0.0` if you want to access on other than localhost)
