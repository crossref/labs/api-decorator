import hashlib
import json
import logging

import boto3
from botocore import UNSIGNED
from botocore.config import Config

# from cr_aws_utils import s3_obj_to_str, s3_to_json_key

BUCKET = "outputs.research.crossref.org"
ANNOTATION_PATH = "annotations"


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

session = boto3.Session()
config = Config(
    signature_version=UNSIGNED
)  # for public access buckets you don't need to sign the request

s3 = session.resource("s3", config=config)
my_bucket = s3.Bucket(BUCKET)


class S3ObjException(Exception):
    pass


def s3_obj_to_str(s3_path):
    logger.info(f"Getting s3 object: {s3_path}")
    try:
        return my_bucket.Object(s3_path).get()["Body"].read().decode("utf-8")
    except Exception as e:
        logger.error(f"Error getting s3 object: {s3_path}")
        logger.error(e)
        raise S3ObjException(e) from e


def s3_to_json_key(key):
    return f'cr-labs-{key.split("/")[-1].split(".")[0]}'


def get_annotations(s3_key):
    return {
        s3_to_json_key(obj.key): json.loads(s3_obj_to_str(obj.key))
        for obj in my_bucket.objects.filter(Prefix=f"{ANNOTATION_PATH}/{s3_key}/")
        if obj.key.endswith(".json")
    }


def parse_key(item_key):
    return item_key[0] if type(item_key) is list else item_key


def extract_key(item, key):
    return None if key is None else f"/{parse_key(item[key])}"


def build_s3_key(item, route, key):
    return f"{route}{extract_key(item, key)}" if key else f"{route}"


def doi_to_md5(doi):
    return hashlib.md5(doi.lower().encode()).hexdigest()


def patch_item(item, route, identifier_key):
    # check to see if it is a DOI, if it is, md5 hash it
    identifier_value = (
        doi_to_md5(item[identifier_key])
        if identifier_key == "DOI"
        else item[identifier_key]
    )
    return get_annotations(f"{route}/{identifier_value}") | item


def annotate_item(response, route, identifier_key):
    response["message"] = patch_item(response["message"], route, identifier_key)
    return response


def annotate_item_list(response, route, key):
    response["message"]["items"] = [
        patch_item(item, route, key) for item in response["message"]["items"]
    ]
    return response


def annotate(response, route, identifier_key):
    return (
        annotate_item_list(response, route, identifier_key)
        if "list" in response["message-type"]
        else annotate_item(response, route, identifier_key)
    )
