import logging
import boto3
from botocore import UNSIGNED
from botocore.config import Config


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

session = boto3.Session()
config = Config(
    signature_version=UNSIGNED
)  # for public access buckets you don't need to sign the request

s3 = session.resource("s3", config=config)


class S3ObjException(Exception):
    pass


def s3_obj_to_str(bucket, s3_path):
    my_bucket = s3.Bucket(bucket)
    logger.info(f"Getting s3 object: {s3_path}")
    try:
        return my_bucket.Object(s3_path).get()["Body"].read().decode("utf-8")
    except Exception as e:
        logger.error(f"Error getting s3 object: {s3_path}")
        logger.error(e)
        raise S3ObjException(e) from e


def s3_to_json_key(key):
    return key.split("/")[-1].split(".")[0]
