import requests


def proxy_api_request(r, headers=None):
    try:
        path = r.url.path
        params = str(r.query_params)
        return requests.get(
            f"https://api.crossref.org{path}", params=params, headers=headers
        ).json()

    except Exception as e:
        raise e
