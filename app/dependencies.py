APP_NAME = "api-decorator"
ABOUT = "A simple API decorator for the Crossref API"
VERSION = "0.1.0"
MAILTO = "labs@crossref.org"
LICENSE_INFO = {
    "name": "MIT License",
    "url": "https://opensource.org/licenses/MIT",
}
TERMS_OF_SERVICE = """
    Warnings, Caveats and Weasel Words:

    This is an experiment running on R&D equipment in a non-production environment.

    It may disappear without warning and/or perform erratically.

    If it isn’t working for some reason, come back later and try again.
"""

SCHEME = "https"
REPRESENTAION_VERSION = "1.0.0"

HEADERS = {"User-Agent": "f{APP_NAME}; mailto:{MAILTO}"}

MEMBER_ID_TILE = "The member ID"
MEMBER_ID_DESCRIPTION = "The member ID as obtained from the REST API"

ISO_DATE_TITLE = "The date of the sample"
ISO_DATE_DESCRIPTION = "The date of the sample in ISO format (YYYY-MM-DD)"

SAMPLE_LIST_TYPE = "sample-uri-list"
DATA_POINTS_LIST_TYPE = "data-points-list"

TYPE_LIST_TYPE = "type-list"
TYPE_SINGLTON_TYPE = "type"

MEMBER_LIST_TYPE = "member-list"
MEMBER_SINGLTON_TYPE = "member"

WORK_LIST_TYPE = "work-list"
WORK_SINGLTON_TYPE = "work"


RELATIONSHIP_LIST_TYPE = "relationship-list"
