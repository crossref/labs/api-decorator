import logging
import time

import sentry_sdk
from aioprometheus import Counter, Gauge, MetricsMiddleware
from aioprometheus.asgi.starlette import metrics
from fastapi import FastAPI, Request, status
from fastapi.responses import HTMLResponse, ORJSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from .dependencies import (
    ABOUT,
    APP_NAME,
    LICENSE_INFO,
    MAILTO,
    TERMS_OF_SERVICE,
    VERSION,
)
from .routers import annotations, samples #, relationships


class ExampleSentryException(Exception):
    pass


# NB you need to set the SENTRY_DSN environment variable
sentry_sdk.init(
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production,
    traces_sample_rate=1.0,
)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

logger.info(f"Starting {APP_NAME} v{VERSION}")


app = FastAPI(
    description=ABOUT,
    title=APP_NAME,
    version=VERSION,
    contact={'email': MAILTO},
    license_info=LICENSE_INFO,
    terms_of_service=TERMS_OF_SERVICE,
)
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

app.include_router(samples.router)
#app.include_router(relationships.router)
app.include_router(annotations.router)

app.state.home_counter = Counter("home_total", "Number home page requests.")

app.add_middleware(MetricsMiddleware)
app.add_route("/metrics", metrics)


@app.get("/", tags=["about"])
async def home(request: Request):
    app.state.home_counter.inc({})
    return templates.TemplateResponse(
        "about.html",
        {
            "request": request,
            "app_name": APP_NAME,
            "version": VERSION,
            "about": ABOUT,
            "terms_of_service": TERMS_OF_SERVICE,
        },
        status_code=status.HTTP_200_OK,
    )


@app.get("/heartbeat", tags=["health"])
async def heartbeat():
    # TODO some actual diagnostics
    return ORJSONResponse(
        {
            "status": "ok",
            "app": APP_NAME,
            "version": VERSION,
            "time": time.time(),
        },
        status_code=status.HTTP_200_OK,
    )


@app.get("/sentry-debug", include_in_schema=False)
async def trigger_error():
    raise ExampleSentryException(
        f"This is an example of an exception being sent to Sentry from {APP_NAME} v{VERSION} at {time.time()}"
    )


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app)
