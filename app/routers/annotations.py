
import logging

from fastapi import APIRouter, Request, Path

from ..annotator import annotate
from ..crapi_proxy import proxy_api_request
from ..dependencies import HEADERS, MEMBER_ID_DESCRIPTION, MEMBER_ID_TILE

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

router = APIRouter()

identifier_keys = {

    "works": "DOI",
    "members": "id",
    "journals": "ISSN",
    "types": "id",
}

@router.get("/{resource_name}/", tags=["proxy and annotate"])
@router.get("/{resource_name}/{identifier}", tags=["proxy and annotate"])
@router.get("/{resource_name}/{identifier}/{other:path}", tags=["proxy and annotate"])
async def proxy_and_annotate(request: Request, resource_name: str = None, identifier: str = None, other: str = None):
    
    logger.info( f"resource: {resource_name} / id_type:{identifier_keys[resource_name]}")
    return annotate(proxy_api_request(request, headers=HEADERS),resource_name, identifier_keys[resource_name])
