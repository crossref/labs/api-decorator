import logging
from datetime import date
from typing import Optional

from fastapi import APIRouter, Path, Request, status
from fastapi.responses import ORJSONResponse, RedirectResponse
import requests


MAINFOLD_URI = "https://manifold-api.production.crossref.org"
# https://manifold-api.production.crossref.org/v2/relationships?uri=https://reddit.com/r/pharmacy/comments/34j1cs/phase_3_trial_of_herpes_zoster_subunit_vaccine/

#from ..crapi_proxy import proxy_api_request
from ..dependencies import (RELATIONSHIP_LIST_TYPE, REPRESENTAION_VERSION, SCHEME)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


router = APIRouter()

def response_header(status, message_type):
    return {
        "status": status,
        "message-type": message_type,
        "message-version": REPRESENTAION_VERSION,
    }

def host_name(request):
    return request.headers.get("x-forwarded-host", request.headers.get("host"))

def message(items):
    return {"message":{"relationships-count": len(items), "items": items}}


@router.get("/relationships/{uri_path:path}")
@router.get("/relationships/{uri_path:path}/", tags=["relationships"])
async def get_relationships(request: Request, uri_path: str | None = None, uri: str | None = None ):
    resource = uri if uri else uri_path
    rel_uri = f"{MAINFOLD_URI}/v2/relationships?uri={resource}"
    return rel_uri
    #items = requests.get(f"{MAINFOLD_URI}/v2/relationships?uri={resource}").json()['relationships']
    

    return ORJSONResponse(
        response_header("ok",RELATIONSHIP_LIST_TYPE) | message(items),
        status_code=status.HTTP_200_OK,
        media_type="application/json"
    )


