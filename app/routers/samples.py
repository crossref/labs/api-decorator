import logging
from datetime import date

from fastapi import APIRouter, Path, Request, status
from fastapi.responses import ORJSONResponse, RedirectResponse


#from ..crapi_proxy import proxy_api_request
from ..dependencies import (DATA_POINTS_LIST_TYPE, ISO_DATE_DESCRIPTION,
                            ISO_DATE_TITLE, MEMBER_ID_DESCRIPTION,
                            MEMBER_ID_TILE, REPRESENTAION_VERSION,
                            SAMPLE_LIST_TYPE, SCHEME)
from ..sampler import (all_member_samples_available,
                       all_member_works_data_points,
                       per_member_samples_available,
                       per_member_works_data_points)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


router = APIRouter()

def response_header(status, message_type):
    return {
        "status": status,
        "message-type": message_type,
        "message-version": REPRESENTAION_VERSION,
    }

def host_name(request):
    return request.headers.get("x-forwarded-host", request.headers.get("host"))

def message(items):
    return {"message":{"data-points-count": len(items), "items": items}}


@router.get("/works/samples", include_in_schema=False)
@router.get("/works/samples/", tags=["samples"])
async def get_all_member_samples_available(request: Request):
    return ORJSONResponse(
        response_header("ok",SAMPLE_LIST_TYPE) | message([f"{SCHEME}://{host_name(request)}/works/samples/{d}/" for d in all_member_samples_available()]),
        status_code=status.HTTP_200_OK,
        media_type="application/json"
    )


@router.get("/works/samples/latest", include_in_schema=False)
@router.get("/works/samples/latest/", tags=["samples"])
async def get_latest_all_member_sample():
    return RedirectResponse(
        f"/works/samples/{all_member_samples_available()[-1]}/",
        status_code=status.HTTP_302_FOUND,
    )


@router.get("/works/samples/{iso_date}", include_in_schema=False)
@router.get("/works/samples/{iso_date}/", tags=["samples"])
async def get_all_member_data_points_for_date(
    iso_date: date = Path(
        ...,
        title=ISO_DATE_TITLE,
        description=ISO_DATE_DESCRIPTION,
    )
):
   
    return ORJSONResponse(
            content=response_header("ok", DATA_POINTS_LIST_TYPE) | message(all_member_works_data_points(iso_date)),
            status_code=status.HTTP_200_OK,
        )

# Catch anything that doesn't have a slash and redirect to the same path with a slash
# @router.get("/members/works/samples/{path_name:path}")
# async def redirect_no_slash(request: Request):
#     return RedirectResponse(
#         f"{request.url.path}/", status_code=status.HTTP_301_MOVED_PERMANENTLY
#     )

@router.get("/members/{member_id}/works/samples", include_in_schema=False)
@router.get("/members/{member_id}/works/samples/", tags=["samples"])
async def get_per_member_samples_available(
    request: Request,
    member_id: str = Path(
        ...,
        title=MEMBER_ID_TILE,
        description=MEMBER_ID_DESCRIPTION,
    ),
):
    return ORJSONResponse(response_header("ok", SAMPLE_LIST_TYPE) | message([f"{SCHEME}://{host_name(request)}/members/{member_id}/works/samples/{iso_date}/" for iso_date in per_member_samples_available(member_id)]) ,status_code=status.HTTP_200_OK,media_type="application/json")

@router.get("/members/{member_id}/works/samples/latest", include_in_schema=False)
@router.get("/members/{member_id}/works/samples/latest/", tags=["samples"])
async def get_latest_per_member_sample(
    member_id: str = Path(
        ...,
        title=MEMBER_ID_TILE,
        description=MEMBER_ID_DESCRIPTION,
    )
):

    return RedirectResponse(
        f"/members/{member_id}/works/samples/{per_member_samples_available(member_id)[-1]}/",
        status_code=status.HTTP_302_FOUND,
    )

@router.get("/members/{member_id}/works/samples/{iso_date}", include_in_schema=False)
@router.get("/members/{member_id}/works/samples/{iso_date}/", tags=["samples"])
async def get_per_member_data_point_for_date(
    member_id: str = Path(title=MEMBER_ID_TILE, description=MEMBER_ID_DESCRIPTION),
    iso_date: date = Path(
        ...,
        title=ISO_DATE_TITLE,
        description=ISO_DATE_DESCRIPTION,
    ),
):

    return ORJSONResponse(
            content=response_header("ok", DATA_POINTS_LIST_TYPE) | message(per_member_works_data_points(member_id,iso_date)),
            status_code=status.HTTP_200_OK,
        )


# # Catch anything that doesn't have a slash and redirect to the same path with a slash
# @router.get("/members/{member_id}/works/samples/{path_name:path}")
# async def redirect_no_slash(request: Request):
#     return RedirectResponse(
#         f"{request.url.path}/", status_code=status.HTTP_301_MOVED_PERMANENTLY
#     )