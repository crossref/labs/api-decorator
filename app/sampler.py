import json
import logging

import boto3
from botocore import UNSIGNED
from botocore.config import Config
from diskcache import FanoutCache

CACHE_TIMEOUT = 30 * 86400  # 30 days


BUCKET = "samples.research.crossref.org"
ANNOTATION_PATH = "annotations"


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

session = boto3.Session()
config = Config(
    signature_version=UNSIGNED
)  # for public access buckets you don't need to sign the request

s3 = session.resource("s3", config=config)
my_bucket = s3.Bucket(BUCKET)

cache = FanoutCache(
    eviction_policy="least-frequently-used",
    size_limit=int(5e9),
    shards=8,
    directory="s3_sample_cache",
)
logger.info(f"Cache size: {cache.size_limit}")


def s3_obj_to_str(s3_path):
    logger.info(f"Getting s3 object: {s3_path}")
    try:
        return my_bucket.Object(s3_path).get()["Body"].read().decode("utf-8")
    except Exception as e:
        logger.error(f"Error getting s3 object: {s3_path}")
        logger.error(e)
        raise S3ObjException(e) from e


class S3ObjException(Exception):
    pass


def extract_date(key, index=-2):
    return key.split("/")[index]


def all_member_samples_available():
    return [
        extract_date(obj.key, -2)
        for obj in my_bucket.objects.filter(Prefix="all-works/20")
        if obj.key.endswith(".jsonl")
    ]


@cache.memoize(expire=CACHE_TIMEOUT)
def all_member_works_data_points(iso_date):
    return [
        json.loads(l)["data-point"]
        for l in s3_obj_to_str(f"all-works/{iso_date}/sample.jsonl").splitlines()
    ]


def per_member_samples_available(member_id):
    return [
        extract_date(l, -3)
        for l in s3_obj_to_str(f"members-works/index/member-{member_id}").splitlines()
    ]


@cache.memoize(expire=CACHE_TIMEOUT)
def per_member_works_data_points(member_id, iso_date):
    return [
        json.loads(l)["data-point"]
        for l in s3_obj_to_str(
            f"members-works/{iso_date}/samples/member-{member_id}.jsonl"
        ).splitlines()
    ]


if __name__ == "__main__":
    print(per_member_works_data_points(78, "2023-02-05"))
