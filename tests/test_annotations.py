from fastapi.testclient import TestClient

from app.dependencies import (
    APP_NAME,
    MEMBER_LIST_TYPE,
    MEMBER_SINGLTON_TYPE,
    TYPE_LIST_TYPE,
    TYPE_SINGLTON_TYPE,
    WORK_LIST_TYPE,
    WORK_SINGLTON_TYPE,
)
from app.main import app

client = TestClient(app)


def test_get_types_noslash():
    response = client.get("/types")
    assert response.status_code == 200
    assert response.json()["message-type"] == TYPE_LIST_TYPE


def test_get_types():
    response = client.get("/types/")
    assert response.status_code == 200
    assert response.json()["message-type"] == TYPE_LIST_TYPE


def test_get_type_noslash():
    response = client.get("/types/book-chapter")
    assert response.status_code == 200
    assert response.json()["message-type"] == TYPE_SINGLTON_TYPE


def test_get_type():
    response = client.get("/types/book-chapter/")
    assert response.status_code == 200
    assert response.json()["message-type"] == TYPE_SINGLTON_TYPE


## members


def test_get_members_noslash():
    response = client.get("/members")
    assert response.status_code == 200
    assert response.json()["message-type"] == MEMBER_LIST_TYPE


def test_get_members():
    response = client.get("/members/")
    assert response.status_code == 200
    assert response.json()["message-type"] == MEMBER_LIST_TYPE


def test_get_member_noslash():
    response = client.get("/members/98")
    assert response.status_code == 200
    assert response.json()["message-type"] == MEMBER_SINGLTON_TYPE


def test_get_member():
    response = client.get("/members/98/")
    assert response.status_code == 200
    assert response.json()["message-type"] == MEMBER_SINGLTON_TYPE


## works


def test_get_works_noslash():
    response = client.get("/works")
    assert response.status_code == 200
    assert response.json()["message-type"] == WORK_LIST_TYPE


def test_get_works():
    response = client.get("/works/")
    assert response.status_code == 200
    assert response.json()["message-type"] == WORK_LIST_TYPE


def test_get_work_noslash():
    response = client.get("/works/10.5555/12345678")
    assert response.status_code == 200
    assert response.json()["message-type"] == WORK_SINGLTON_TYPE


def test_get_work():
    response = client.get("/works/10.5555/12345678/")
    assert response.status_code == 200
    assert response.json()["message-type"] == WORK_SINGLTON_TYPE


# /members/98/works

# @pytest.mark.parametrize("sample", inject_test_dois(file="peak_multires.tsv"))
# def test_get_works(sample):
#     response = client.get(f"/?doi={sample.doi}")
