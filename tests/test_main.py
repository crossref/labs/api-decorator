from fastapi.testclient import TestClient
from app.dependencies import APP_NAME

from app.main import app

client = TestClient(app)


def test_get_main():
    response = client.get("/")
    assert response.status_code == 200
    assert APP_NAME in response.text


def test_get_heartbeat():
    response = client.get("/heartbeat")
    assert response.status_code == 200
    assert APP_NAME in response.text
