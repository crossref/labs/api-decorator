from fastapi.testclient import TestClient

from app.dependencies import APP_NAME, DATA_POINTS_LIST_TYPE, SAMPLE_LIST_TYPE
from app.main import app

client = TestClient(app)


def test_get_all_member_samples_no_slash():
    response = client.get("/works/samples")
    assert response.status_code == 200
    assert response.json()["message-type"] == SAMPLE_LIST_TYPE


def test_get_all_member_samples():
    response = client.get("/works/samples/")
    assert response.status_code == 200
    assert response.json()["message-type"] == SAMPLE_LIST_TYPE


def test_get_latest_all_member_datapoints_no_redirect_no_slash():
    response = client.get("/works/samples/latest", follow_redirects=False)
    assert response.status_code == 302


def test_get_latest_all_member_datapoints_no_redirect():
    response = client.get("/works/samples/latest/", follow_redirects=False)
    assert response.status_code == 302


def test_get_latest_all_member_datapoints_no_slash():
    response = client.get("/works/samples/latest")
    assert response.status_code == 200
    assert response.json()["message-type"] == DATA_POINTS_LIST_TYPE


def test_get_latest_all_member_datapoints():
    response = client.get("/works/samples/latest/")
    assert response.status_code == 200
    assert response.json()["message-type"] == DATA_POINTS_LIST_TYPE


def test_get_per_member_samples_no_slash():
    response = client.get("/members/98/works/samples")
    assert response.status_code == 200
    assert response.json()["message-type"] == SAMPLE_LIST_TYPE


def test_get_per_member_samples():
    response = client.get("/members/98/works/samples/")
    assert response.status_code == 200
    assert response.json()["message-type"] == SAMPLE_LIST_TYPE


def test_get_latest_per_member_datapoints_no_redirect_noslash():
    response = client.get("/members/98/works/samples/latest", follow_redirects=False)
    assert response.status_code == 302


def test_get_latest_per_member_datapoints_no_redirect():
    response = client.get("/members/98/works/samples/latest/", follow_redirects=False)
    assert response.status_code == 302


def test_get_latest_per_member_datapoints_noslash():
    response = client.get("/members/98/works/samples/latest")
    assert response.status_code == 200
    assert response.json()["message-type"] == DATA_POINTS_LIST_TYPE


def test_get_latest_per_member_datapoints():
    response = client.get("/members/98/works/samples/latest/")
    assert response.status_code == 200
    assert response.json()["message-type"] == DATA_POINTS_LIST_TYPE
